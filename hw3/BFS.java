import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class BFS 
{
	// Returns the set of reachable locations using breadth first search
	public static Set<Location> getReachableSet(WeightedGraph graph, Location start)
	{
	// Problem #1
	// Fill in this method to compute the set of all possible reachable 
	// locations (ignoring costs and budget).  You must use Breadth
	// First Search to get full credit.
		
		//Set holding the vertices that are confirmed visited
		Set<Location> visited = new HashSet<Location>();
		
		//Queue holding the next level of vertices
		LinkedList<Location> choices = new LinkedList<Location>();
		
		//Add the start vertex to both the confirmed and potential
		choices.add(start);
		visited.add(start);
		
		//Loop until there are no more potential vertices
		while(choices.size() != 0)
		{
			//Make start the next item in the queue
			start = choices.poll();
			
			//Create iterator for the neighbors of new start
			Iterator<Location> i = graph.getNeighbors(start).iterator();
			while(i.hasNext())
			{
				//Add the neighbors to visited and next up if not already 
				//in there
				Location temp = i.next();
				if(!visited.contains(temp))
				{
					visited.add(temp);
					choices.add(temp);
				}
			}
		}
		
		//Return the confirmed visited list
		return visited;
	}

}
