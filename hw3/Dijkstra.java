import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Dijkstra 
{
	// Returns the set of destinations that can be reached without going over budget
	public static Set<Location> getDestinationSet(WeightedGraph graph, Location start, Integer budget)
	{
	// Problem #2
	// Fill in this method to compute the set of all possible destinations 
	// that can be reached while staying within the travel budget. You must
	// use Dijkstra's algorithm to get full credit.  We encourage you to
	// implement the |V|^2 time version of Dijkstra's algorithm.  You are
	// free to add helper methods. 
		
		//Set that will hold the final tree
		Set<Location> tree = new HashSet<Location>();
		
		//Map that holds the weight from source to vertices added to final tree
		Map<Location, Integer> weights = new HashMap<Location, Integer>();
		//Map that holds potential vertices and their weights from source
		Map<Location, Integer> choices = new HashMap<Location, Integer>();
		
		//Add starting vertex to the final set, as well as into the weights set
		tree.add(start);
		weights.put(start, 0);
		
		//Add the neighbors of the starter to the choices map
		Iterator<Location> starter = graph.getNeighbors(start).iterator();
		while(starter.hasNext())
		{
			Location temp = starter.next();
			choices.put(temp, graph.getWeight(temp, start));
		}
		
		//Outside loop that goes to the next vertex with smallest weight
		while(budget > 0)
		{
			//Use helper method that selects the next smallest weight
			Location smallest = minDistance(choices);
			//Subtract new weight from the budget; if less than 0, stop algorithm
			budget -= choices.get(smallest);
			if(budget < 0)
				return tree;
			
			//Add the next smallest vertex to the final tree and 
			//its weight to the weights map, as well as remove it from choices
			tree.add(smallest);
			weights.put(smallest, choices.get(smallest));
			choices.remove(smallest);
			
			//Iterate through neiboring vertices
			Iterator<Location> i = graph.getNeighbors(smallest).iterator();
			while(i.hasNext())
			{
				Location temp = i.next();
				//Only visit vertices that are not already in 
				//final tree
				if(!tree.contains(temp))
				{
					//We find a vertex that is already in  our potential list
					if(choices.containsKey(temp))
					{
						int currentWeight = choices.get(temp);
						int weightToSmallest = weights.get(smallest);
						int weightFromSmallestToTemp = graph.getWeight(temp, smallest);
						int newWeight = weightToSmallest + weightFromSmallestToTemp;
						
						if(newWeight < currentWeight)
						{
							choices.replace(temp, newWeight);
						}
					}
					//Find a new vertex
					else
					{
						int smallestWeight = weights.get(smallest);
						int weightBetween = graph.getWeight(smallest, temp);
						choices.put(temp, smallestWeight + weightBetween);
					}
				}
			}
		}
		
		return tree;
	}
	
	//Helper method that picks the next vertex with the smallest weight to the source
	private static Location minDistance(Map<Location, Integer> toAdd)
	{		
		Location smallest = new Location("");
		int smallestWeight = Integer.MAX_VALUE;
		
		for(Location l: toAdd.keySet())
		{
			if(toAdd.get(l) < smallestWeight)
			{
				smallest = l;
				smallestWeight = toAdd.get(l);
			}
		}
		
		return smallest;
	}
	
}
