import java.util.Arrays;

public class Selector 
{
	
	private static void swap(Word[] array, int i, int j)
	{
		if(i == j) return;
		
		Word temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	public static Word[] select(Word[] array, int k)
	{
	// Problem #1
	// Fill in this method with an O(n*k) time algorithm
	// that returns the k largest elements of array in
	// order from largest to smallest.
	// Note: This should return an array with k elements
		
		//Calculate the left and right side to pass to sorting method
		int l = 0;
		int r = array.length - 1;
		Selector.sort(array, l, r);
		
		//Copy the first 500 elements into a new array to return
		return Arrays.copyOfRange(array, 0, k - 1);
	}
	
	//Helper method to sort the array to go with merge sort
	private static void sort(Word[] array, int l, int r)
	{
		//Make sure array has elements
		if(l < r)
		{
			//find halfway point of array
			int m = (l + r) / 2;
			
			//pass each half recursively back to sort
			Selector.sort(array, l, m);
			Selector.sort(array, m + 1, r);
			
			//merge the two halves created
			Selector.merge(array, l, m, r);
		}
	}
	
	//Helper method that merges and sorts two halves of the passed array
	private static void merge(Word[] array, int l, int m, int r)
	{
		//Get number elements in each half
		int n1 = m - l + 1;
		int n2 = r - m;
		
		//Make new temp arrays to sort
		Word left[] = new Word[n1];
		Word right[] = new Word[n2];
		
		//Copy each element into its corresponding half
		for(int i = 0; i < n1; i++)
		{
			left[i] = array[l + i];
		}
		for(int j = 0; j  < n2; j++)
		{
			right[j] = array[m + 1 + j];
		}
		
		//Initialize counters for loops
		int i = 0, j = 0;
		int k = l;
		
		//Loop through array and put the greater element in the current main array spot
		while(i < n1 && j < n2)
		{
			if(left[i].compareTo(right[j]) >= 0)
			{
				array[k] = left[i];
				i++;
			}
			else
			{
				array[k] = right[j];
				j++;
			}
			k++;
		}
		
		//Copy the remaining elements into the main array
		while(i < n1)
		{
			array[k] = left[i];
			i++;
			k++;
		}
		while(j < n2)
		{
			array[k] = right[j];
			j++;
			k++;
		}
	}
}
