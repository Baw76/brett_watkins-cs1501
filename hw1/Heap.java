public class Heap 
{
	//Heap is the global heap with this class; n is number of elements
	public Word[] heap;
	public int n;
	
	public Heap(Word[] array)
	{
		//Initialize global variables and send the array to the heap builder
		heap = array;
		n = heap.length;
		buildHeap(heap);
	}
	
	public void buildHeap(Word[] array)
	{
	// Problem #2
	// Fill in this method with an O(n) time algorithm
	// that builds an n element complete binary heap.
	// Note: You are allowed to add and modify fields
    // and helper methods.
		
		//Make an itial loop that loops through the subtrees and making them heaps;
		//Then heaps a level subtree above
		for(int i = n / 2 - 1; i >= 0; i--)
		{
			heapify(i);
		}
	}
	
	public Word removeMax()
	{
	// Problem #3
	// Fill in this method with an O(log(n)) time algorithm
	// that removes the root element, restores the heap
	// structure, and finally returns the removed root element.
		
		//Get the top element
		Word max;
		
		//Make sure the heap contains elements
		if(n < 1)
		{
			return null;
		}
		else
		{
			//Replace the max with the last element and then heapify it
			max = heap[0];
			heap[0] = heap[n - 1];
			n--;
			heapify(0);
			return max;
		}
	}
	
	public Word[] select(int k)
	{
		Word[] result = new Word[k];
		for(int i = 0; i < k; i++){
			result[i] = this.removeMax();
		}
		return result;
	}
	
	//Helper method to put elements in their correct position
	//Does it by rising up the tree and making subtrees into heaps
	//One level at a time
	private void heapify(int i)
	{
		//Variables
		int left, right, greaterC;
		Word temp;
		
		//Get the left and right children of the current index
		left = 2 * i + 1;
		right = 2 * i + 2;
		
		//If the left child is greater then update it
		if(left < n && heap[left].compareTo(heap[i]) > 0)
		{
			greaterC = left;
		}
		else
		{
			greaterC = i;	
		}
		
		//Check if the right child is greater than the left or parent; update it if so
		if(right < n && heap[right].compareTo(heap[greaterC]) > 0)
		{
			greaterC = right;
		}
		
		//If the parent was switched we need to update the actual data in the array
		if(greaterC != i)
		{
			temp = heap[i];
			heap[i] = heap[greaterC];
			heap[greaterC] = temp;
			
			//Now recurse and do the next level of heapify
			heapify(greaterC);
		}
	}
}
