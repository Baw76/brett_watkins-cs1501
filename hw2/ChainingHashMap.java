
public class ChainingHashMap
{
	Node[] array;
	int size;
	
	public ChainingHashMap(int size)
	{
		this.size = size;
		array = new Node[size];
	}

	public Integer get(Word key) 
	{
	// Problem #1A
	// Fill in this method to get the value corresponding
	// with the key. Note: if the key is not found, then
	// return null.
		
		//Initialize variables
		int index = key.hashCode() % array.length;
		Node currentNode = array[index];
		
		//Loop through list checking for the key; if found output;
		//If not found move to next node
		while(currentNode != null)
		{
			if(key.equals(currentNode.word))
			{
				return currentNode.frequency;
			}
			
			currentNode = currentNode.next;
		}
		
		return null;
	}

	public void put(Word key, Integer value) 
	{
	// Problem #1B
	// Fill in this method to insert a new key-value pair into
	// the map or update the existing key-value pair if the
	// key is already in the map.`
	
		//Initialize variables
		int index = key.hashCode() % array.length;
		Node currentNode = array[index];
		
		//Check if the list is empty and if it is add the new node as root
		if(currentNode == null)
		{
			array[index] = new Node(key, value, null);
			return;
		}
		
		//Loop through list checking for the key; if found update value
		while(currentNode != null)
		{
			if(key.equals(currentNode.word))
			{
				currentNode.frequency = value;
				return;
			}
			
			//If this is final node, add new node as next with key 
			//Becuase this is final node and we have not found key
			if(currentNode.next == null)
			{
				currentNode.next = new Node(key, value, null);
				return;
			}
			currentNode = currentNode.next;
		}
	}

	public Integer remove(Word key) 
	{
	// Problem #1C
	// Fill in this method to remove a key-value pair from the
	// map and return the corresponding value. If the key is not
	// found, then return null.
	
		//Initialize variables
		int index = key.hashCode() % array.length;
		Node currentNode = array[index];
		Node previous = null;
		
		//If list is empty return
		if(currentNode == null)
			return null;
		
		//Loop through list checking for key
		while(currentNode != null)
		{
			if(key.equals(currentNode.word))
			{
				//Make a temp to store the value of the key so we can delete 
				//the node
				Integer temp = currentNode.frequency;
				
				//If this list is only one long make the list empty
				if(previous == null)
				{
					currentNode = null;
					return temp;
				}
				//If we are removing the final node, make the previous 
				//node's next null 
				if(currentNode.next == null)
				{
					previous.next = null;
					currentNode = null;
					return temp;
				}
				
				previous.next = currentNode.next;
				currentNode = null;
				return temp;
			}
			
			previous = currentNode;
			currentNode = currentNode.next;
		}
		
		return null;
	}
	
	// This method returns the total size of the underlying array.
	// In other words, it returns the total number of linkedlists.
	public int getSize(){
		return size;
	}
	
	// This method counts how many keys are stored at the given array index.
	// In other words, it computes the size of the linkedlist at the given index.
	public int countCollisions(int index)
	{
		if(index < 0 || index >= size) return -1;
		
		int count = 0;
		Node temp = array[index];
		while(temp != null){
			temp = temp.next;
			count++;
		}
		
		return count;
	}
	
}
