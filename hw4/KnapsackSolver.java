
public class KnapsackSolver 
{

	public static int[][] buildTable(Order[] orders, int costLimit, int timeLimit)
	{
	// Problem #1
	// Fill in this method to create a (costLimit + 1) by (timeLimit + 1) table
	// that for each (i, j) stores the maximum number of cookies that can be
	// produced with cost at most i in time at most j.
		//Initialize answer
		int[][] cookieTable = new int[costLimit + 1][timeLimit + 1];
		
		//Loop through the array
		for(int i = 0; i < costLimit; i++)
		{
			for(int j = 0; j < timeLimit; j++)
			{
				//Base case
				if(i == 0 || j == 0)
					cookieTable[i][j] = 0;
				else
				{
					//Loop through the orders and add ones that fit
					for(int temp = 0; temp < orders.length; temp++)
					{
						if(orders[temp].cost < i && orders[temp].time < j)
						{
							cookieTable[i][j] = orders[temp].numberOfCookies + cookieTable[i - orders[temp].cost][j - orders[temp].time];
						}
					}
					
					//If nothing was added then simply make the best answer
					//Equal to that of the previous
					if(cookieTable[i][j] == 0)
						cookieTable[i][j] = cookieTable[i-1][j-1];
				}
			}
		}
		
		return cookieTable;
	}

	public static Multiset solve(Order[] orders, int costLimit, int timeLimit)
	{
	// Problem #2
	// Fill in this method to create a multiset that represents a combination of
	// cookie choices that maximizes the number of cookies with cost at most 
	// costLimit in time at most timeLimit.  Note: You can call buildTable as
	// a subroutine.
		//Initialize variables
		Multiset setOfCookies = new Multiset();
		int[][] cookieTable = buildTable(orders, costLimit, timeLimit);
		int max = cookieTable[costLimit][timeLimit];
		int added = 0;
		
		//Sort the orders array in descending order
		for(int i = 0; i < orders.length - 1; i++)
		{
			for(int j = 1; j < orders.length; j++)
			{
				if(orders[j].numberOfCookies > orders[i].numberOfCookies)
				{	
					Order temp = orders[j];
					orders[j] = orders[i];
					orders[i] = temp;
				}
			}
		}
		//Loop until we reach the max
		int counter = 0;
		while(added < max)
		{
			if(orders[counter].numberOfCookies + added < max)
			{
				setOfCookies.add(orders[counter]);
				added += orders[counter].numberOfCookies;
			}
			counter++;
		}
		
		
		return setOfCookies;
	}
}